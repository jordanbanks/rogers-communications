Veteran Sports, Tech and Media executive. Excellent strategic thinker with a flair for innovation and finding creative solutions to challenging problems. Proven ability to lead, win and drive results in start-up and mature businesses both locally and globally. In-demand industry speaker and media interviewee. Community leader who inspires through action.

Website: http://www.rogers.com